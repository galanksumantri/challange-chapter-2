const mainColors = {
  blue1: '#091B6F',
  blue2: '#D3D9FD',
  green1: '#5CB85F',
  green2: '#DEF1DF',
  grey1: '#E9E9E9',
  grey2: '#D3D9FD',
};

export const colors = {
  white: 'white',
  background: mainColors.blue1,
  border: mainColors.grey1,
  background2: mainColors.grey2,
  text: {
    primary: '#000000',
    secondary: mainColors.green1,
    menuInactive: 'black',
    white: 'white',
    menuActive: mainColors.blue1,
  },
  button: {
    primary: mainColors.green1,
    secondary: mainColors.green2,
  },
};
