import IcOlehOleh from './IcOlehOleh.svg'
import IcPenginapan from './IcPenginapan.svg'
import IcSewaMobil from './IcSewaMobil.svg'
import IcWisata from './IcWisata.svg'

export {IcOlehOleh, IcPenginapan, IcSewaMobil, IcWisata}
